
const onFormSubmit = () => {
    const form = document.getElementById('registerForm'); // agarrar form del html
    postToServer(form).then((data) => {
        // data.success responde el servidor, es un booleano
        if (data.success) {
            //window.location.href = "index.html";
            // MOSTRAR ALERTA PARA SUCCESS
            let x = document.getElementById("alerta-success");
            x.classList.remove('invisible');
            x.classList.add("visible");
            form.reset();
            setTimeout(() => {
                x.classList.replace('visible', 'invisible');
            }, 5000);
        } else {
            // MOSTRAR ALERTA ERROR
            let x = document.getElementById("alerta-error");
            x.classList.remove('invisible');
            x.classList.add("visible");
            setTimeout(() => {
                x.classList.replace('visible', 'invisible');
            }, 4000);
        }
    });
}

const getFormValues = (form) => {
    const formValue = new Object(); // objeto para valores individuales
    const elementsArray = Array.from(form.elements) // valores en arreglo
    // iterar para crear objeto a enviar al servidor
    elementsArray.forEach(element => {
        if (element.name) formValue[`${element.name}`] = element.value;
    })
    return formValue; // retorna el objeto con los valores
}

const postToServer = async (form) => {
    try {
        const formValue = getFormValues(form); // agarrar valores del form
        // el fetch se hace a la URL del backend;
        let res = await fetch('http://localhost:8080/api/pacientes', {
            'method': 'POST',
            'body': JSON.stringify(formValue),
            headers: {
                'Content-Type': 'application/json',
            }
        });
        let data = res.json();
        return data
    } catch (error) {
        console.log('Hubo un error al fetchear los datos. Revisar conexión a BD', error);
        return
    }
}