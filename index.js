let patients = [];
let canNavigate = true;


const getAllPatients = async () => {
    // traer pacientes del servidor
    try {
        // GET al endpoint
        let res = await fetch("http://localhost:8080/api/pacientes", {
            headers: {
                "Content-Type": "application/json",
            },
        });
        let data = res.json(); // pasa la respuesta a JSON.
        return data;
    } catch (error) {
        console.log(
            "Hubo un error al fetchear los datos. Revisar conexión a BD",
            error
        );
        return;
    }
};

// Guardar en el localStorage el último item clickeado;
// y navegar
const saveLastCodigoClicked = (codigo) => {
    if (canNavigate) {
        // seteo el item en el localStorage
        localStorage.setItem('codigo', codigo);
        // navega a el detalle del paciente
        window.location.href = "detalle-paciente.html";
    }
}

getAllPatients().then((data) => {
    patients = data;

    const getTableData = patients
        // mapear a HTML para mostrar en la interfaz
        .map((paciente) => {
            // Formatear fecha para el usuario
            let fecha_nacimiento = new Date(paciente.fecha_nacimiento).toLocaleDateString();
            return `<tr class="hover:bg-blue-300 hover:cursor-pointer" onclick="saveLastCodigoClicked(${paciente.codigo})">
                <td class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">${paciente.codigo}</td>
                <td class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">${paciente.nombre}</td>
                <td class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">${paciente.apellido}</td>
                <td class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">${fecha_nacimiento}</td>
                <td class="whitespace-nowrap px-4 py-2 font-medium text-gray-900" onclick="onDeletePaciente(${paciente.codigo})">
                    <img src="delete.png" width="32" alt="Italian Trulli">
                </td>
            </tr>`;
        })
        .join("");

    const tableBody = document.querySelector("#tableBody"); // agarrar la tabla
    tableBody.innerHTML = getTableData; // popular la tabla
});

const onDeletePaciente = async (codigoPaciente) => {
    canNavigate = false;
    let opcion = confirm("Está seguro que desea borrar el paciente? Esta acción es irreversible");
    if (opcion == true) {
        try {
            let res = await fetch(`http://localhost:8080/api/pacientes/${codigoPaciente}`, {
                method: 'DELETE',
            });
            let data = res.json();
            window.location.href = "index.html";
            canNavigate = false;
            return data;
        } catch (error) {
            console.log(
                "Hubo un error al fetchear los datos. Revisar conexión a BD",
                error
            );
            return;
        }
    } else {
        canNavigate = false;
        window.location.href = "index.html";
    }
};

// escuchar inputs del usuario
document.getElementById('filter').addEventListener('input', (e) => filter_table(e));

// filtrar tabla a medida que el user escribe el input;
function filter_table(e) {
    let value = e.target.value;
    let noData = document.getElementById('no-data');
    if (e.target.value === '') {
        console.log('none');
        noData.style.display = 'none';
    } else {
        noData.style.display = 'none';
    }
    value.toLowerCase(); // pasar a minusculas
    const rows = document.querySelectorAll('tbody tr') // agarrar los rows de la tabla
    rows.forEach(row => {
        // Si el valor buscado no está en la tabla, ocultar
        row.style.display = (row.innerText.toLowerCase().includes(value)) ? '' : 'none'
        // animacion de no encontrado
        noData.style.display = (!row.innerText.toLowerCase().includes(value)) ? 'flex' : 'none'
    })
}